#! /usr/bin/env python3
import numpy as np, argparse
import pandas as pd
from scipy.spatial.distance import pdist, squareform

###############

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def get_bondbond_orientional(pols=None, pol_size=None, steps=None):
    # first get the bond vectors
    bonds = np.diff(pols, axis=1)
    bonds_dist = np.array([1. - pdist(b0, 'cosine') for b0 in bonds])
    mean_bonds_dist = np.mean(bonds_dist, axis=0)
    mean_bonds_dist_matrix = squareform(mean_bonds_dist)
    res = np.array([mean_bonds_dist_matrix.diagonal(k).mean() for k in steps])
    return res
###############


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input hdf file')
    parser.add_argument('-o', type=str, help='output filename')
    parser.add_argument('--t1', type=float, default=0.2)
    parser.add_argument('--t2', type=float, default=1.0)
    parser.add_argument('--lags', type=int, default=2000)
    parser.add_argument('--step', type=float, default=0)
    args = parser.parse_args()

    # read the input file
    rcm_df = pd.read_hdf(args.i, key='rcm')
    start = int(len(rcm_df)*args.t1)
    end = int(len(rcm_df)*args.t2)
    if not(args.step>0):
        step=1
    elif args.step>=1.0:
        step = len(rcm_df) - 1
    else:
        step = int(len(rcm_df)*args.step)
    lags = args.lags

    print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))
    pols = np.zeros((rcm_df.shape[1], rcm_df.shape[0], 3))
    for p0 in rcm_df.columns:
        pols[int(p0[3:])] = np.vstack(rcm_df[p0].values)
    corr_pol = pd.DataFrame()
    idummy = 0
    steps = np.arange(1, lags+1)
    printProgressBar(idummy, rcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    for k in rcm_df.columns:
        printProgressBar(idummy, rcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
        pol0 = np.stack(rcm_df[k].values[start:])
        polid = int(k[3:])
        corr_pol[k] = get_bondbond_orientional([pols[polid, start:end:step]], steps=steps)
        idummy += 1
    corr_pol['mean'] = corr_pol.mean(axis=1)
    printProgressBar(idummy, rcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    corr_pol.to_csv(args.o, sep='\t')
