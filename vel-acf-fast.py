#! /usr/bin/env python3
import numpy as np, argparse
import pandas as pd

###############

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

###############


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input hdf file')
    parser.add_argument('-o', type=str, help='output filename')
    parser.add_argument('--t1', type=float, default=0.2)
    parser.add_argument('--t2', type=float, default=1.0)
    parser.add_argument('--lags', type=int, default=2000)
    parser.add_argument('--step', type=float, default=0)
    args = parser.parse_args()

    # read the input file
    vcm_df = pd.read_hdf(args.i, key='vcm')
    start = int(len(vcm_df)*args.t1)
    end = int(len(vcm_df)*args.t2)
    if not(args.step>0):
        step=1
    elif args.step>=1.0:
        step = len(vcm_df) - 1
    else:
        step = int(len(vcm_df)*args.step)
    lags = args.lags
    print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))

    corr_pol = pd.DataFrame()
    idummy = 0
    printProgressBar(idummy, vcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    for k in vcm_df.columns:
        printProgressBar(idummy, vcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
        pol0 = np.stack(vcm_df[k].values[start:])
        corr0 = np.zeros((2*lags -1, 3))

        for i in range(3):
            pol0i = pol0[:, i]
            pol0i2 = [pol0i[0:lags]]
            for j in np.arange(step, pol0i.shape[0]-lags+step, step):
                pol0i2 += [pol0i[j:j+lags]]
            pol0i2 = np.array(pol0i2)

            corr0[:, i] = np.zeros(2*lags-1)
            for p0 in pol0i2:
                corr0[:, i] += np.correlate(p0, p0, mode='full')
            corr0[:, i] /= pol0i2.shape[0]
        corr_pol[k] = np.sum(corr0[corr0.shape[0]//2:, :], axis=1)
        idummy += 1
    printProgressBar(idummy, vcm_df.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    corr_pol.to_csv(args.o, sep='\t')
