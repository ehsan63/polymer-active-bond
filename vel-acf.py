import numpy as np, argparse
import pandas as pd

###############

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def acf(x, t):
    return np.mean([np.dot(x[i], x[i+t])/np.dot(x[i],x[i]) for i in np.arange(0, len(x)-t)])


###############

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', type=str, help='input hdf file')
    parser.add_argument('-o', type=str, help='output filename')
    #parser.add_argument('--mode', type=str, help='rcm, rg2, re2, vcm, msid or all', dest='mode', default='rg2')
    parser.add_argument('--t1', type=float, default=0.2)
    parser.add_argument('--t2', type=float, default=1.0)
    parser.add_argument('--lags', type=int, default=2000)
    #parser.add_argument('--step', type=float, default=0)
    args = parser.parse_args()

    # read the input file
    vcm = pd.read_hdf(args.i, key='vcm')
    acf_vals_all = np.zeros((args.lags, vcm.shape[1]))

    lags = np.arange(args.lags)
    start = int(len(vcm)*args.t1)
    end = int(len(vcm)*args.t2)
    print('t1 {}: snapshot {}, t2 {}: snapshpt {}'.format(args.t1, start, args.t2, end))

    idummy = 0
    printProgressBar(idummy, vcm.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
    for i in np.arange(vcm.shape[1]):
        printProgressBar(idummy, vcm.shape[1], prefix = 'Progress:', suffix = 'Complete', length = 20, fill=r'|')
        vcm0 = np.stack(vcm.iloc[start:, i])
        acf_vals_all[:, i] = np.array([acf(vcm0, l0) for l0 in lags])
        idummy += 1
    acf_df = pd.DataFrame(acf_vals_all, index=lags)
    acf_df.to_csv(args.o, sep='\t')
